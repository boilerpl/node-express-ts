// Import with 3 interfaces Express, Request and Response
import express, {Express, Request, Response} from "express";
import dotenv from "dotenv";

// Instanciate express app
const app: Express = express();
dotenv.config();

// Server port
const port = process.env.PORT;

// Default route
app.get('/', (req: Request, res: Response)=>{
    res.send('Hi, i am listening on PORT 2456')
})

// Start listening to requests
app.listen(port);