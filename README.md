# App Name

## Description

## Installation

## Usage

### Privacy

## Support

## Feedback

Any feedback, suggestions or proper collaboration attempts are very welcome! Especially for other projects!

## Roadmap

Set up initial framework to work with

## Authors and acknowledgment

Contributors: Cyaton

### Acknowledgments

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Active development. Readme last updated 2023
